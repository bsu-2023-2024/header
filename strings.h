#pragma once
namespace Strings
{
	const int maxLength = 256;
	void getUserString(char arr[], int);
	char toUpper(char symbol);
	bool isAlfa(char symbol);
	bool equals(char lhs[], char rhs[], bool caseSensetive);
	int compare(char lhs[], char rhs[], bool caseSensetive);
	int firstEntrance(char arr1[], char arr2[]);
	int getLength(char arr[]);
	void displayArray(char arr[]);
	void swap(char& a, char& b);
	void replaceSubstring(char source[], char toChange[], char changeOn[]);
};
