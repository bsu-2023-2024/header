#include <iostream>
#include "strings.h"

void Strings::getUserString(char arr[], int maxLength)
{
    std::cout << "Enter some text, less than " << maxLength << " symbols: ";
    std::cin.getline(arr, maxLength);
}

char Strings::toUpper(char symbol)
{
    return symbol <= 'z' && symbol >= 'a' ? symbol - 32 : symbol;
}

bool Strings::isAlfa(char symbol)
{
    return symbol >= 'A' && symbol <= 'Z' || symbol >= 'a' && symbol <= 'z';
}

bool Strings::equals(char lhs[], char rhs[], bool caseSensetive = 0)
{
    if (strlen(lhs) != strlen(rhs))
    {
        return false;
    }

    for (int i = 0; lhs[i]; i++)
    {
        char lhSymbol = caseSensetive ? lhs[i] : toUpper(lhs[i]);
        char rhSymbol = caseSensetive ? rhs[i] : toUpper(rhs[i]);

        if (lhSymbol != rhSymbol)
        {
            return false;
        }
    }
    return true;
}

int Strings::compare(char lhs[], char rhs[], bool caseSensetive = 0)
{
    size_t length = strlen(lhs) <= strlen(rhs) ? strlen(lhs) : strlen(rhs);

    for (size_t i = 0; length; i++)
    {
        char lhSymbol = caseSensetive ? lhs[i] : toUpper(lhs[i]);
        char rhSymbol = caseSensetive ? rhs[i] : toUpper(rhs[i]);

        if (lhSymbol - rhSymbol > 0)
        {
            return 1;
        }
        else if (lhSymbol - rhSymbol < 0)
        {
            return -1;
        }
    }

    if (strlen(lhs) - strlen(rhs) == 0)
    {
        return 0;
    }
    else if (strlen(lhs) - strlen(rhs) > 0)
    {
        return 1;
    }
    else
    {
        return -1;
    }

}

int Strings::firstEntrance(char string[], char substring[])
{
    int entranceIndex = 0;
    bool isEnter = false;
    for (int i = 0; string[i]; i++)
    {
        for (int j = 0; substring[j]; j++)
        {
            if (substring[j] == string[i + j])
            {
                isEnter = true;
                entranceIndex = i;
                if (!substring[j + 1])
                {
                    return entranceIndex;
                }
            }
            else
            {
                isEnter = false;
                entranceIndex = 0;
                break;
            }
        }
    }

    return -1;
}

int Strings::getLength(char arr[])
{
    int i = 0;
    while (arr[i] != 0)
    {
        i++;
    }
    return i;
}

void Strings::displayArray(char arr[])
{
    for (size_t i = 0; arr[i]; i++)
    {
        std::cout << arr[i] << " ";
    }
}

void Strings::swap(char& a, char& b)
{
    char tmp = a;
    a = b;
    b = tmp;
}

void Strings::replaceSubstring(char source[], char toChange[], char changeOn[])
{
    int sourceLength = getLength(source);
    int entrance = firstEntrance(source, toChange);
    int toChangeLength = getLength(toChange);
    int changeOnLength = getLength(changeOn);
    int difference = toChangeLength - changeOnLength;
    if (difference >= 0)
    {
        for (size_t i = entrance + difference - 1; i < sourceLength - difference; i++)
        {
            source[i] = source[i + difference];
        }
        source[sourceLength - difference] = '\0';
    }
    else
    {
        for (size_t i = sourceLength + 1; i > entrance + toChangeLength; i--)
        {
            source[i] = source[i + difference + 1];
        }
    }
    for (size_t i = 0; i < changeOnLength; i++)
    {
        source[entrance + i] = changeOn[i];
    }

}

