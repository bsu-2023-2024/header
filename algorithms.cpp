#include <iostream>
#include "algorithms.h"

void swap(int& a, int& b)
{
    char tmp = a;
    a = b;
    b = tmp;
}

void exchangeSort(int numbers[], int length)
{
    bool swapped = false;
    int alreadySorted = 0;
    do
    {
        swapped = false;
        for (size_t i = 1; i < length - alreadySorted; i++)
        {
            if (numbers[i - 1] > numbers[i])
            {
                swap(numbers[i - 1], numbers[i]);
                swapped = true;
            }
        }
        alreadySorted++;
    } while (swapped);
}

void selectionSort(int numbers[], int length)
{
    int lastNotSorted = 0;
    int current_minimum = 0;

    for (size_t i = 0; i < length - 1; i++)
    {
        current_minimum = lastNotSorted;
        for (size_t j = lastNotSorted; j < length; j++)
        {
            if (numbers[current_minimum] > numbers[j])
            {
                current_minimum = j;
            }
        }
        swap(numbers[lastNotSorted], numbers[current_minimum]);
        lastNotSorted++;
    }
}

void insertionSort(int numbers[], int length)
{
    for (size_t i = 1; i < length; i++)
    {
        for (int j = i - 1; j >= 0; j--)
        {
            if (numbers[j + 1] < numbers[j])
            {
                swap(numbers[j + 1], numbers[j]);
            }
            else
            {
                break;
            }
        }
    }
}


void sieveOfEratosthenes(int numbers[], int length, int elementIndex)
{
    if (elementIndex <= sqrt(length))
    {
        if (numbers[elementIndex] != 0)
        {
            for (size_t i = elementIndex * 2; i < length; i += elementIndex)
            {
                numbers[i] = 0;
            }
        }
        sieveOfEratosthenes(numbers, length, elementIndex + 1);
    }
}

void displayArray(int arr[], int length)
{
    for (size_t i = 0; i < length; i++)
    {
        std::cout << arr[i] << " ";
    }
}

int FibonachiNumber(int n, int first, int second)
{
    if (n == 2)
    {
        return second;
    }
    return FibonachiNumber(n - 1, second, second + first);
}

int binarySearch(int sortedArr[], int length, int element)
{
    int first = 0;
    int last = length - 1;

    while (first < last)
    {
        int middle = (last + first) / 2;

        if (element < sortedArr[middle])
        {
            last = middle - 1;
        }
        else if (element > sortedArr[middle])
        {
            first = middle + 1;
        }
        else
        {
            return middle;
        }
    }

    if (first == last)
    {
        if (sortedArr[last] == element)
        {
            return last;
        }
        else
        {
            return -1;
        }
    }
}
