#pragma once
class algorithms
{
	void exchangeSort(int arr[], int);
	void swap(int& a, int& b);
	void selectionSort(int arr[], int);
	void insertionSort(int arr[], int);
	void sieveOfEratosthenes(int numbers[], int, int);
	void displayArray(int arr[]);
	int FibonachiNumber(int n, int first, int second);
	int binarySearch(int sortedArr[], int length, int element);
};